#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <time.h>

#include "bcm_host.h"

#include "GLES2/gl2.h"
#include "EGL/egl.h"
#include "EGL/eglext.h"

#include "revision.h"
#include "dashgl.h"

#define LCD 0

typedef struct {
	uint32_t screen_width;
	uint32_t screen_height;

	EGLDisplay display;
	EGLSurface surface;
	EGLContext context;

	GLuint program;
	GLuint buf;
	GLuint vshader;
	GLuint fshader;
	
	GLuint vbo_cube_vertices;
	GLuint ibo_cube_elements;
	
	GLint texture_id;
	GLint attribute_coord3d, attribute_texcoord, uniform_mvp, uniform_mytexture;
	float fade;
	float fade_dx;
	unsigned int rot;
} GLOBAL_T;

static void init_ogl(GLOBAL_T *state);
static void init_shaders(GLOBAL_T *state);
static void draw_triangles(GLOBAL_T *state);

int main () {

	int terminate = 0;
	bcm_host_init();
	GLOBAL_T state;

	if (get_processor_id() == PROCESSOR_BCM2838) {
		puts("This demo application is not available on the Pi4\n\n");
		exit(0);
	}

	// Clear application state
	memset( &state, 0, sizeof( GLOBAL_T ) );

	state.fade = 1.0f;
	state.fade_dx = -0.01f;
	state.rot = 0;

	// Start OGLES
	init_ogl(&state);
	init_shaders(&state);

	while (!terminate) {
		draw_triangles(&state);
	}

	return 0;
}

static void init_ogl(GLOBAL_T *state) {

	int major, minor;
	EGLint num_config;
	static EGL_DISPMANX_WINDOW_T nativewindow;
	DISPMANX_ELEMENT_HANDLE_T dispman_element;
	DISPMANX_DISPLAY_HANDLE_T dispman_display;
	DISPMANX_UPDATE_HANDLE_T dispman_update;
	VC_RECT_T dst_rect;
	VC_RECT_T src_rect;
	EGLConfig config;

	static const EGLint attribute_list[] = {
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
		EGL_ALPHA_SIZE, 8,
		EGL_DEPTH_SIZE, 16,
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_NONE
	};
	
	static const EGLint context_attributes[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};

	// get an EGL display connection
	state->display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
	if(state->display == EGL_NO_DISPLAY) {
		fprintf(stderr, "Failed to get EGL display! Error: %s\n", eglGetErrorStr());
		exit(EXIT_FAILURE);
	}

	// initialize the EGL display connection
	if (eglInitialize(state->display, &major, &minor) == EGL_FALSE) {
		fprintf(stderr, "Failed to get EGL version! Error: %s\n", eglGetErrorStr());
		eglTerminate(state->display);
		exit(EXIT_FAILURE);
	}

	printf("Initialized EGL version: %d.%d\n", major, minor);

	// get an appropriate EGL frame buffer configuration
	if(eglChooseConfig(state->display, attribute_list, &config, 1, &num_config) == EGL_FALSE) {
		fprintf(stderr, "Failed to get EGL Config! Error: %s\n", eglGetErrorStr());
		eglTerminate(state->display);
		exit(EXIT_FAILURE);
	}

	// get an appropriate EGL frame buffer configuration
	if(eglBindAPI(EGL_OPENGL_ES_API) == EGL_FALSE) {
		fprintf(stderr, "Failed to bind OpenGL API! Error: %s\n", eglGetErrorStr());
		eglTerminate(state->display);
		exit(EXIT_FAILURE);
	}

	// create an EGL rendering context
	state->context = eglCreateContext(state->display, config, EGL_NO_CONTEXT, context_attributes);
	if(state->context == EGL_NO_CONTEXT) {
		fprintf(stderr, "Failed to get EGL Context! Error: %s\n", eglGetErrorStr());
		eglTerminate(state->display);
		exit(EXIT_FAILURE);
	}

	// create an EGL window surface
	if(graphics_get_display_size(LCD, &state->screen_width, &state->screen_height) < 0) {
		fprintf(stderr, "Failed to get display size!\n");
		eglTerminate(state->display);
		exit(EXIT_FAILURE);
	}

	dst_rect.x = 0;
	dst_rect.y = 0;
	dst_rect.width = state->screen_width;
	dst_rect.height = state->screen_height;
		
	src_rect.x = 0;
	src_rect.y = 0;
	src_rect.width = state->screen_width;
	src_rect.height = state->screen_height;

	dispman_display = vc_dispmanx_display_open(LCD);
	dispman_update = vc_dispmanx_update_start( 0 );
			
	dispman_element = vc_dispmanx_element_add ( 
		dispman_update, 
		dispman_display,
		0/*layer*/, 
		&dst_rect, 
		0/*src*/,
		&src_rect, 
		DISPMANX_PROTECTION_NONE, 
		0 /*alpha*/, 
		0/*clamp*/, 
		0/*transform*/
	);
		
	nativewindow.element = dispman_element;
	nativewindow.width = state->screen_width;
	nativewindow.height = state->screen_height;
	vc_dispmanx_update_submit_sync( dispman_update );

	state->surface = eglCreateWindowSurface( state->display, config, &nativewindow, NULL );
	if(state->surface == EGL_NO_SURFACE) {
		fprintf(stderr, "Failed to create surface! Error: %s\n", eglGetErrorStr());
		eglTerminate(state->display);
		exit(EXIT_FAILURE);
	}

	// connect the context to the surface
	if(eglMakeCurrent(state->display, state->surface, state->surface, state->context) == EGL_FALSE) {
		fprintf(stderr, "Failed to make current! Error: %s\n", eglGetErrorStr());
		eglDestroySurface(state->display, state->surface);
		eglTerminate(state->display);
		exit(EXIT_FAILURE);
	}
	

}


static void init_shaders(GLOBAL_T *state) {

	float *shark_vertices;
	uint32_t i, file_size;
	FILE *fp = fopen("shark.bin", "rb");
	fseek(fp, 0, SEEK_END);
	file_size = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	shark_vertices = malloc(file_size);
	fread(shark_vertices, 1, file_size, fp);
	fclose(fp);

	for(i = 0; i < 20; i++) {
		printf("%d) %0.2f\n", i, shark_vertices[i]);
	}

	glGenBuffers(1, &state->vbo_cube_vertices);
	glBindBuffer(GL_ARRAY_BUFFER, state->vbo_cube_vertices);
	glBufferData(GL_ARRAY_BUFFER, file_size, shark_vertices, GL_STATIC_DRAW);
	free(shark_vertices);

	state->texture_id = dgl_texture_load("texture.png");
	state->program = dash_create_program("shaders/vertex.glsl", "shaders/fragment.glsl");
	if(!state->program) {
		exit(EXIT_FAILURE);
	}

	const char* attribute_name;
	attribute_name = "coord3d";
	state->attribute_coord3d = glGetAttribLocation(state->program, attribute_name);
	if (state->attribute_coord3d == -1) {
		fprintf(stderr, "Could not bind attribute %s\n", attribute_name);
		exit(EXIT_FAILURE);
	}

	attribute_name = "texcoord";
	state->attribute_texcoord = glGetAttribLocation(state->program, attribute_name);
	if (state->attribute_texcoord == -1) {
		fprintf(stderr, "Could not bind attribute %s\n", attribute_name);
		exit(EXIT_FAILURE);
	}

	const char* uniform_name;
	uniform_name = "mvp";
	state->uniform_mvp = glGetUniformLocation(state->program, uniform_name);
	if (state->uniform_mvp == -1) {
		fprintf(stderr, "Could not bind uniform %s\n", uniform_name);
		exit(EXIT_FAILURE);
	}

	uniform_name = "mytexture";
	state->uniform_mytexture = glGetUniformLocation(state->program, uniform_name);
	if (state->uniform_mytexture == -1) {
		fprintf(stderr, "Could not bind uniform %s\n", uniform_name);
		exit(EXIT_FAILURE);
	}

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glDepthMask(GL_TRUE);
	glClearDepthf(1.0f);
	glDepthRangef(0.1f, 1.0f);

}

static void draw_triangles(GLOBAL_T *state) {
	
	glBindFramebuffer(GL_FRAMEBUFFER,0);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glBindBuffer(GL_ARRAY_BUFFER, state->buf);
	glUseProgram ( state->program );

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, state->texture_id);
	glUniform1i(state->uniform_mytexture, 0);

	time_t current_time;
	current_time = time(NULL);
	float rad = state->rot * M_PI / 180.0;
	state->rot++;

	if(state->rot == 360) {
		state->rot = 0;
	}

	vec3 eye = { 0.0f, 2.0f, 0.0f };
	vec3 target = { 0.0f, 0.0f, -4.0f };
	vec3 axis = { 0.0f, 1.0f, 0.0f };
	vec3 t = { 0.0, 0.0, -4.0f };

	mat4 mvp, pos, rot, projection, view;
	mat4_identity(mvp);
	mat4_perspective(45.0f, 1.0f*state->screen_width/state->screen_height, 0.1f, 10.0f, projection);
	mat4_look_at(eye, target, axis, view);
	mat4_translate(t, pos);
	mat4_rotate_y(rad, rot);

	mat4_multiply(mvp, projection, mvp);
	mat4_multiply(mvp, view, mvp);
	mat4_multiply(mvp, pos, mvp);
	mat4_multiply(mvp, rot, mvp);

	glUniformMatrix4fv(state->uniform_mvp, 1, GL_FALSE, mvp);

	glBindBuffer(GL_ARRAY_BUFFER, state->vbo_cube_vertices);
	glEnableVertexAttribArray(state->attribute_coord3d);
	glVertexAttribPointer(
    	state->attribute_coord3d, 
    	3, 
    	GL_FLOAT,  
    	GL_FALSE, 
    	sizeof(float)*5, 
    	0 
  	);

	glEnableVertexAttribArray(state->attribute_texcoord);
	glVertexAttribPointer(
    	state->attribute_texcoord,
    	2, 
    	GL_FLOAT,
    	GL_FALSE,
    	sizeof(float)*5,
    	(void*)(sizeof(float)*3)
  	);

	glDrawArrays ( GL_TRIANGLES, 0, 1284 );
	glDisableVertexAttribArray(state->attribute_coord3d);
	glDisableVertexAttribArray(state->attribute_texcoord);

	glFlush();
	glFinish();
	eglSwapBuffers(state->display, state->surface);

}


