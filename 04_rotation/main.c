#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <time.h>

#include "bcm_host.h"

#include "GLES2/gl2.h"
#include "EGL/egl.h"
#include "EGL/eglext.h"

#include "revision.h"
#include "dashgl.h"

#define LCD 0

typedef struct {
	uint32_t screen_width;
	uint32_t screen_height;

	EGLDisplay display;
	EGLSurface surface;
	EGLContext context;

	GLuint program;
	GLuint buf;
	GLuint vshader;
	GLuint fshader;
	GLint attribute_coord2d, attribute_v_color, uniform_mvp;
	float fade;
	float fade_dx;
	unsigned int rot;
} GLOBAL_T;

static void init_ogl(GLOBAL_T *state);
static void init_shaders(GLOBAL_T *state);
static void draw_triangles(GLOBAL_T *state);

int main () {

	int terminate = 0;
	bcm_host_init();
	GLOBAL_T state;

	if (get_processor_id() == PROCESSOR_BCM2838) {
		puts("This demo application is not available on the Pi4\n\n");
		exit(0);
	}

	// Clear application state
	memset( &state, 0, sizeof( GLOBAL_T ) );

	state.fade = 1.0f;
	state.fade_dx = -0.01f;
	state.rot = 0;

	// Start OGLES
	init_ogl(&state);
	init_shaders(&state);

	while (!terminate) {
		draw_triangles(&state);
	}

	return 0;
}

static void init_ogl(GLOBAL_T *state) {

	int major, minor;
	EGLint num_config;
	static EGL_DISPMANX_WINDOW_T nativewindow;
	DISPMANX_ELEMENT_HANDLE_T dispman_element;
	DISPMANX_DISPLAY_HANDLE_T dispman_display;
	DISPMANX_UPDATE_HANDLE_T dispman_update;
	VC_RECT_T dst_rect;
	VC_RECT_T src_rect;
	EGLConfig config;

	static const EGLint attribute_list[] = {
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
		EGL_ALPHA_SIZE, 8,
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_NONE
	};
	
	static const EGLint context_attributes[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};

	// get an EGL display connection
	state->display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
	if(state->display == EGL_NO_DISPLAY) {
		fprintf(stderr, "Failed to get EGL display! Error: %s\n", eglGetErrorStr());
		exit(EXIT_FAILURE);
	}

	// initialize the EGL display connection
	if (eglInitialize(state->display, &major, &minor) == EGL_FALSE) {
		fprintf(stderr, "Failed to get EGL version! Error: %s\n", eglGetErrorStr());
		eglTerminate(state->display);
		exit(EXIT_FAILURE);
	}

	printf("Initialized EGL version: %d.%d\n", major, minor);

	// get an appropriate EGL frame buffer configuration
	if(eglChooseConfig(state->display, attribute_list, &config, 1, &num_config) == EGL_FALSE) {
		fprintf(stderr, "Failed to get EGL Config! Error: %s\n", eglGetErrorStr());
		eglTerminate(state->display);
		exit(EXIT_FAILURE);
	}

	// get an appropriate EGL frame buffer configuration
	if(eglBindAPI(EGL_OPENGL_ES_API) == EGL_FALSE) {
		fprintf(stderr, "Failed to bind OpenGL API! Error: %s\n", eglGetErrorStr());
		eglTerminate(state->display);
		exit(EXIT_FAILURE);
	}

	// create an EGL rendering context
	state->context = eglCreateContext(state->display, config, EGL_NO_CONTEXT, context_attributes);
	if(state->context == EGL_NO_CONTEXT) {
		fprintf(stderr, "Failed to get EGL Context! Error: %s\n", eglGetErrorStr());
		eglTerminate(state->display);
		exit(EXIT_FAILURE);
	}

	// create an EGL window surface
	if(graphics_get_display_size(LCD, &state->screen_width, &state->screen_height) < 0) {
		fprintf(stderr, "Failed to get display size!\n");
		eglTerminate(state->display);
		exit(EXIT_FAILURE);
	}

	dst_rect.x = 0;
	dst_rect.y = 0;
	dst_rect.width = state->screen_width;
	dst_rect.height = state->screen_height;
		
	src_rect.x = 0;
	src_rect.y = 0;
	src_rect.width = state->screen_width;
	src_rect.height = state->screen_height;

	dispman_display = vc_dispmanx_display_open(LCD);
	dispman_update = vc_dispmanx_update_start( 0 );
			
	dispman_element = vc_dispmanx_element_add ( 
		dispman_update, 
		dispman_display,
		0/*layer*/, 
		&dst_rect, 
		0/*src*/,
		&src_rect, 
		DISPMANX_PROTECTION_NONE, 
		0 /*alpha*/, 
		0/*clamp*/, 
		0/*transform*/
	);
		
	nativewindow.element = dispman_element;
	nativewindow.width = state->screen_width;
	nativewindow.height = state->screen_height;
	vc_dispmanx_update_submit_sync( dispman_update );

	state->surface = eglCreateWindowSurface( state->display, config, &nativewindow, NULL );
	if(state->surface == EGL_NO_SURFACE) {
		fprintf(stderr, "Failed to create surface! Error: %s\n", eglGetErrorStr());
		eglTerminate(state->display);
		exit(EXIT_FAILURE);
	}

	// connect the context to the surface
	if(eglMakeCurrent(state->display, state->surface, state->surface, state->context) == EGL_FALSE) {
		fprintf(stderr, "Failed to make current! Error: %s\n", eglGetErrorStr());
		eglDestroySurface(state->display, state->surface);
		eglTerminate(state->display);
		exit(EXIT_FAILURE);
	}
	

}


static void init_shaders(GLOBAL_T *state) {

	static const GLfloat vertex_data[] = {
		 0.0,  0.5, 1.0, 1.0, 0.0,
		 -0.5, -0.5, 0.0, 0.0, 1.0,
		 0.5, -0.5, 1.0, 0.0, 0.0
	};
	
	state->vshader = dgl_create_shader("shaders/vertex.glsl", GL_VERTEX_SHADER);
	state->fshader = dgl_create_shader("shaders/fragment.glsl", GL_FRAGMENT_SHADER);

	if(state->vshader == 0 || state->fshader == 0) {
		fprintf(stderr, "Error compiling shader\n");
		exit(EXIT_FAILURE);
	}

	state->program = glCreateProgram();
	glAttachShader(state->program, state->vshader);
	glAttachShader(state->program, state->fshader);
	glLinkProgram(state->program);


	// Prepare viewport
	glViewport ( 0, 0, state->screen_width, state->screen_height );
	glClearColor ( 0.0, 0.0, 0.0, 1.0 );
		  
	// Upload vertex data to a buffer
	glGenBuffers(1, &state->buf);
	glBindBuffer(GL_ARRAY_BUFFER, state->buf);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data, GL_STATIC_DRAW);

	const char *attribute_name = "coord2d";
	state->attribute_coord2d = glGetAttribLocation(state->program, attribute_name);
	if(state->attribute_coord2d == -1) {
		fprintf(stderr, "Could not bind attribute %s\n", attribute_name);
		return;
	}
	
	attribute_name = "v_color";
	state->attribute_v_color = glGetAttribLocation(state->program, attribute_name);
	if(state->attribute_v_color == -1) {
		fprintf(stderr, "Could not bind attribute %s\n", attribute_name);
		return;
	}

	const char *uniform_name = "mvp";
	state->uniform_mvp = glGetUniformLocation(state->program, uniform_name);
	if(state->uniform_mvp == -1) {
		fprintf(stderr, "Could not bind uniform %s\n", uniform_name);
		return;
	}

}

static void draw_triangles(GLOBAL_T *state) {
	
	glBindFramebuffer(GL_FRAMEBUFFER,0);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glBindBuffer(GL_ARRAY_BUFFER, state->buf);
	glUseProgram ( state->program );

	time_t current_time;
	current_time = time(NULL);
	float rad = state->rot * M_PI / 180.0;
	state->rot++;

	if(state->rot == 360) {
		state->rot = 0;
	}

	state->fade += state->fade_dx;
	if(state->fade <= -1.0f) {
		state->fade = -1.0f;
		state->fade_dx = 0.01f;
	} else if(state->fade >= 1.0f) {
		state->fade = 1.0f;
		state->fade_dx = -0.01f;
	}

	mat4 mvp, pos, rot;
	vec3 t = { state->fade, 0.0f, 0.0f };
	mat4_identity(mvp);
	mat4_rotate_z(rad, rot);
	mat4_translate(t, pos); 
	mat4_multiply(mvp, pos, mvp);
	mat4_multiply(mvp, rot, mvp);

	glUniformMatrix4fv(state->uniform_mvp, 1, GL_FALSE, mvp);

	glEnableVertexAttribArray(state->attribute_coord2d);
	glEnableVertexAttribArray(state->attribute_v_color);

	glVertexAttribPointer(
		state->attribute_coord2d,
		2,
		GL_FLOAT,
		GL_FALSE,
		sizeof(float) * 5,
		0
	);

	glVertexAttribPointer(
		state->attribute_v_color,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(float) * 5,
		(void*)(sizeof(float) * 2)
	);
	
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glDisableVertexAttribArray(state->attribute_coord2d);
	glDisableVertexAttribArray(state->attribute_v_color);

	glFlush();
	glFinish();
	eglSwapBuffers(state->display, state->surface);

}


