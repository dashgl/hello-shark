void main(void) {

	vec4 color = vec4(1.0, 0.0, 0.0, 1.0);
	int x = int(gl_FragCoord.x);
		
    if(x > 400) {
        color = vec4(0.0, 1.0, 0.0, 1.0);
    } else {
       	color = vec4(0.0, 0.0, 1.0, 1.0); 
	}

	gl_FragColor = color;

}
