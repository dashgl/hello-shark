# DashGL Season 2 Tutorial 1 : Hello Shark

This is the first set of tutorials for Season 2 of DashGL. In this season we
transition from using OpenGL 3 with GTK on the desktop to using OpenGL 2es with
embedded graphics on the Raspberry Pi. The advantage of this approach is that
is will allow us to create programs that will be launchable from EmulationStation
on RetroPie. You can watch the trailer for this set of tutorials below.

[![DashGL Season 2 Tutorial 1 Trailer](https://i.imgur.com/wjg57Pk.png)](https://www.youtube.com/watch?v=HKeEJ7WNDWo)

## The Structure of this Respository

This repository is structured as a list of lessons. Each lessons is contained in
its own folder. And Each folder contains a file named ```build.sh``` which can
be run to compile each example. And each example can then be run with the binary 
file ```a.out```. Each folder will also contain its own ```README.md``` file for
more information about each lesson respectively.

A quick note about the examples is that they expect a headless enrionment to run
in. So the output of the examples may not be visible from a desktop or from RetroPie
without a start script. So to test these examples it it recomended that you run
```sudo raspi-config```, and from the menu select ```3 Boot Options``` then 
```B1 Desktop / CLI``` and then choose the option ```B1 Console```.

Two things to mention if you are running these tutorials from RetroPie, is that
you will need the Raspberry Pi 2 or 3 images as the Pi 1 / 0 images do not seem
to compile. I don't know what the issue is and will need to look into why. Another
note for RetroPie is that the default option to start Emulation Station is using
the ```B2 Console Autologin``` option from the ```raspi-config``` boot options. 
So if you have disabled auto-login for development, you can restore the defaults
with this option.

## What's needed to run these tutorials?

To run these tutorials you will need a Raspberry Pi 2 or 3 running RetroPie with
the Raspberry Pi 2/3 image and a display. The display can be HDMI, and I have only
tested one kind of GPIO display, but these tutorials were able to display on that.
This will be the target device.

Since the output of the examples take up the entire screen and run from the command
line it's recomended that you have a second device which is a standard desktop.
This can be a Linux computer, Mac, another Raspberry Pi running a dekstop, or a
Windows computer using Cygwin or Putty. Basically this computer will be used to
SSH into your target device, but it makes development a lot more comfortable.